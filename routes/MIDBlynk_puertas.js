var express=require('express');
var router = express.Router();

var puertas_controller = require('../controllers/MIDBlynk_puertas');

router.post('/puerta',puertas_controller.puertas);
router.get('/status',puertas_controller.status);

module.exports = router;