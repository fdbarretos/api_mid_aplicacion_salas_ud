'use stricts'

let chai = require('chai');
let chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url= 'http://localhost:3030';


describe('Insertar una sala 500: ',()=>{

	it('deberia insertar una sala', (done) => {
		chai.request(url)
			.post('/puerta')
			.send({sala:"500", accion: 1, autor: "Fabian"})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});

describe('Insertar una sala 601: ',()=>{

	it('deberia insertar una sala', (done) => {
		chai.request(url)
			.post('/puerta')
			.send({sala:"601", accion: 1, autor: "Fabian"})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});

describe('Insertar una sala 701: ',()=>{

	it('deberia insertar una sala 701', (done) => {
		chai.request(url)
			.post('/puerta')
			.send({sala:"701", accion: 1, autor: "Fabian"})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});

describe('Insertar una sala no existente: ',()=>{

	it('No insertar una sala, no existe', (done) => {
		chai.request(url)
			.post('/puerta')
			.send({sala:"600", accion: 1, autor: "Fabian"})
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});
